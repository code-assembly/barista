// http://eslint.org/docs/user-guide/configuring
module.exports = {
  root: true,
  env: {
    browser: true,
  },

  parserOptions: {
    'parser': 'babel-eslint',
    'ecmaVersion': 2017,
    'sourceType': 'module'
  },

  extends: [
    // add more generic rulesets here:
    'plugin:vue/recommended'
  ],

  rules: {
    // override/add rules settings here:
    "vue/max-attributes-per-line": [2, {
      "singleline": 3,
      "multiline": {
        "max": 1,
        "allowFirstLine": false
      }
    }],

    "vue/html-indent": ["error", "tab", {
      "attribute": 1,
      "closeBracket": 0,
      "ignores": []
    }],

    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
  },

}